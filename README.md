# [sekretar.net](https://sekretar.net) source codes

<br/>

### Run sekretar.net on localhost

    # vi /etc/systemd/system/sekretar.net.service

Insert code from sekretar.net.service

    # systemctl enable sekretar.net.service
    # systemctl start sekretar.net.service
    # systemctl status sekretar.net.service

http://localhost:4053
